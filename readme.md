# Vanillabot

TeamSpeak3 bot handling sub channels creation on demand and then destruction once it's empty. It uses Node.js.

## Overview

I have a teamspeak server in which there are channels for games (e.g. "CS:GO", "Rocket League", etc). They each have their own sub channels for each different parties playing at the same time (e.g. "CS:GO Match 1", "CS:GO Match 2", ..., "Rocket League Match 1", "Rocket League Match 2", etc). It can be annoying to have all that space taken up by 2 or more empty sub channels, So I decided to write this bot to automate the creation and destruction on demand. Basically, if you put a keyword you defined in a channel's topic (default is "dynamique"), joining that same channel makes the bot creates a sub channel named "Salon X" where X is the highest number currently used (channel name prefix defaults to "Salon " but can be changed in the config.

## How to use Vanillabot

### Set up config.json
 
```json
{
        "host": "127.0.0.1",
        "port": 10011,
        "serverId": 1,
        "login": "serveradmin",
        "password": "",
        "tickInterval": 60,
        "botName": "vanillabot",
        "statistics": {
                "keyword": "statistiques",
                "idleTime": 15
        },
        "afkAutoMove": {
                "keyword": "afk:",
                "idleTime": 30
        },
        "autoChannelCreation": {
                "keyword": "dynamique",
                "channelPrefix": "Salon ",
                "inheritedPermissionIds" : [
                        "i_channel_needed_join_power",
                        "i_channel_needed_subscribe_power",
                        "i_channel_needed_description_view_power",
                        "i_channel_needed_modify_power",
                        "i_channel_needed_delete_power",
                        "i_ft_needed_file_browse_power",
                        "i_ft_needed_file_upload_power",
                        "i_ft_needed_file_download_power",
                        "i_ft_needed_file_rename_power",
                        "i_ft_needed_directory_create_power",
                        "i_icon_id"
                ]
        }
}
```

Vanillabot detects channels it's supposed to handle by looking at their topics, it looks for the **keyword** property you set up in the config. This is supposed to be used as a tag in the future, as Vanillbot may use more than one tag, so basically, channel topic just has to contain what you set up in **keyword** (Note that it's case sensitive).
You can set up a number of permissions you want your sub channel to inherit using the **inheritedPermissionIds**.

if you want to use the afkAutoMove feature, you have to to put the keyword you defined immediatly followed by the channel number you want people to be put in once afk. example in this case afk:1234 would move people to channel 1234 after 30 (idleTime) minutes.

The rest of the config.json is pretty self-explanatory. Be sure to rename config.sample.json to config.json before starting it.

### Install dependencies

```shell-script
$ npm install
```

### Start it

```shell-script
$ node vanillabot.js
```
### Using the startup script

There's a startup script I use for all my other node applications, just copy it

```shell-script
cp startup_script.sample.sh startup_script.sh
```

and edit it the APP_DIR variable to adjust to your own path. I use monit to ensure it is restarted in case of crash, here is a sample monit conf (example using /opt/vanillabot/ as a base path)

```conf
check process vanillabot with pidfile /opt/vanillabot/pid/app.pid
  start program = "/opt/vanillabot/startup_script.sh start" as uid 1000 and gid 1000
  stop  program = "/opt/vanillabot/startup_script.sh stop" as uid 1000 and gid 1000
```

Of course you can adjust uid and gid according to your own needs.

## Features that may be added

### Usage statistics and automatic reordering

 On our ts3 server, we like players to use the proper channels whenever possible, we don't want players to play rocket league on the cs:go channels. So basically the idea would be to have some sort of gamification by showing a top 5 of the most played game in the last 24/72/168h (or something like that)

#### Automatic reordering

Using the statistics to reorder channels.

### AFK automover

As the title implies, auto moving people to an *AFK* channel.

### Web interface

for configuring / monitoring purposes, as ts3 textchat is a boring alternative


## Credits

Vanillabot uses [node-teamspeak](https://github.com/gwTumm/node-teamspeak)
