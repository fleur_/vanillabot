var tsc = require("node-teamspeak");
var util = require("util");
var config = require("./config.json");

var cl = new tsc(config.host, config.port);
var dynamicChannels = [];
var lastNotifyMsg = "";

cl.send("login", {client_login_name: config.login, client_login_password: config.password}, function (err, response) {
	cl.send("use", {sid: config.serverId}, function (err, response) {
		cl.send("servernotifyregister", { event:"channel", id:0 }, function (err, clientmoved) {
			cl.send("clientupdate", {client_nickname: config.botName}, function (err, response) {
				tick();
			});
		});
	});
});

cl.on("channeledited", function(client) {});
cl.on("clientmoved", enterChannel);
cl.on("cliententerview", enterChannel);

function enterChannel(client) {
	let strMsg = JSON.stringify(client);

	if (lastNotifyMsg === JSON.stringify(client)) return;

	lastNotifyMsg = JSON.stringify(client);

	let currentChannel = null;

	cl.send("channellist", ['topic'], function (err, channelList) {

		channelList.forEach(function (channel) {
			if ((client.ctid === channel.cid) && channel.channel_topic.includes(config.autoChannelCreation.keyword))
				currentChannel = channel;
		});

		if (currentChannel === null) return;

		let channelNameLookup = config.autoChannelCreation.channelPrefix;
		let channelMatching = [];
		let nextNumber = null;

		channelList.forEach(function (channel) {
			if ((channel.channel_name.includes(channelNameLookup)) && (currentChannel.cid == channel.pid)) {
				channelMatching.push(parseInt(channel.channel_name.substring(channelNameLookup.length),10));
			}
		});

		if (channelMatching.length == 0)
			nextNumber = 1;
		else {
			channelMatching = channelMatching.sort((a,b) => a-b).reverse();
			nextNumber = channelMatching[0] + 1;
		}

		let newName = channelNameLookup + nextNumber;
		let cpid = currentChannel.cid;

		createChannel({
			channel_name: newName,
			channel_flag_permanent: 1,
			channel_flag_temporary: 0,
			channel_topic: "temporary",
			cpid: cpid,
			callback: function(err, newChannel) {
				cl.send("channelpermlist", ['permsid'], {cid:cpid }, function (err, permissions) {
					let permsToInherit = [];
					permissions.forEach( permission => {
						if (config.autoChannelCreation.inheritedPermissionIds.includes(permission.permsid)) {
							permsToInherit.push("permsid=" + permission.permsid + " permvalue=" + permission.permvalue)
						}
					});
					cl.send("channeladdperm cid=" + newChannel.cid + " " +  permsToInherit.join("|"), function (err, permissions) {
						cl.send("clientmove", {clid:client.clid, cid:newChannel.cid }, function (err, response) {
							cl.send("channeledit", {cid: newChannel.cid, channel_flag_permanent: 0, channel_flag_temporary: 1}, function (err, response) {
							});
						});
					});
				});
			}
		});
	});
}

function createChannel(obj) {

	let config = {
		channel_name: obj.channel_name,
		channel_flag_permanent: obj.channel_flag_permanent,
		channel_flag_temporary: obj.channel_flag_temporary,
		channel_topic: obj.channel_topic,
		cpid: obj.cpid
	};

	if ('channel_order' in obj)
		config.channel_order = obj.channel_order;

	cl.send("channelcreate", config, obj.callback);
}

function channelsToTree(channels, clients) {
	let map = {}
	let tree = [];

	channels.forEach((channel, i) => {
		map[channel.cid] = i;
		channel.client_count = 0;
		channel.afk_clients = [];
		channel.children = [];
		clients.forEach( client => {
			if ((client.client_idle_time < config.statistics.idleTime*60*1000) && 
					(client.client_type === 0) &&
					(client.cid == channel.cid))
				channel.client_count++;
			if ((client.client_idle_time > config.afkAutoMove.idleTime*60*1000) && 
					(client.client_type === 0) &&
					(client.cid == channel.cid))
				channel.afk_clients.push(client.clid);
		});
	});

	channels.forEach((channel, i) => {
		if (channel.pid !== 0) {
			channels[map[channel.pid]].children.push(channel);
		} else {
			tree.push(channel);
		}
	});

	return tree;
}

function countClients(channel) {
	let count = 0;
	channel.children.forEach(channel => {
		count += countClients(channel);
	});
	return (channel.client_count + count);
}

function getAfkClients(channel) {
	let list = [];
	channel.children.forEach(channel => {
		list = list.concat(getAfkClients(channel));
	});
	return (channel.afk_clients.concat(list));
}

function tick() {
	/*
	cl.send("version", function (err, version) {
		setTimeout(tick, config.tickInterval * 1000);
	});
*/
	cl.send("channellist", ['topic'], function (err, channels) {
		cl.send("clientlist", ['times'], function (err, clients) {
			if (typeof clients === 'object') clients = [clients];
			let channelTree = channelsToTree(channels, clients);
			let clientsToMove = [];
			let moveToCid = null;
			
			channelTree.forEach((channel) => {
				if (channel.channel_topic.includes(config.statistics.keyword)) {
					console.log(countClients(channel));
				}
				if (channel.channel_topic.includes(config.afkAutoMove.keyword)) {
					let regex = new RegExp(config.afkAutoMove.keyword + "([0-9]+)", "g");
					let results = channel.channel_topic.match(regex);
					if (results !== null)
					{
						moveToCid = parseInt(results[0].substring(config.afkAutoMove.keyword.length), 10 );
						
						clientsToMove = clientsToMove.concat(getAfkClients(channel));
					}
				}
			});
			if (clientsToMove.length !== 0) {
				let clientsStr = [];
				
				clientsToMove.forEach((clients) => {
					clientsStr.push("clid=" + clients);
				});
				
				let str = "clientmove " + clientsStr.join("|") + " cid=" + moveToCid;
				
				cl.send(str, function (err, response) {
					
				});
			}
			setTimeout(tick, config.tickInterval * 1000);
		});
	});
}
